import React, { Component } from "react";
import { Grid, Cell, List, ListItem, ListItemContent } from "react-mdl";

const Contact = () => {
	return (
		<div className="contact-body">
			<Grid className="contact-grid">
				<Cell col={6}>
					<h2>Philippe A. Mallari</h2>
					<img
						src="images/me.PNG"
						alt="avatar"
						style={{ height: "250px" }}
					/>
					<p
						style={{
							width: "75%",
							margin: "auto",
							paddingTop: "1em"
						}}
					>
						Hi! I'm Philippe Mallari, I'm passionate about web
						developing and technology on the side I also love sports
						and playing RTS games! Collaborate with me! Let's turn
						your ideas into reality.
					</p>
				</Cell>
				<Cell col={6}>
					<h2>Contact Me</h2>
					<hr />
					<div className="contact-list">
						<List>
							<ListItem>
								<ListItemContent
									id="list-id"
									style={{
										fontSize: "30px",
										fontFamily: "Anton"
									}}
								>
									<i
										className="fa fa-phone-square"
										aria-hidden="true"
									/>
									0906 336 5462
								</ListItemContent>
							</ListItem>

							<ListItem>
								<ListItemContent
									id="list-id"
									style={{
										fontSize: "30px",
										fontFamily: "Anton"
									}}
								>
									<i class="fas fa-home"></i>
									Quezon City, Philippines
								</ListItemContent>
							</ListItem>

							<ListItem>
								<ListItemContent
									id="list-id"
									style={{
										fontSize: "30px",
										fontFamily: "Anton"
									}}
								>
									<i
										className="fa fa-envelope"
										aria-hidden="true"
									/>
									mallari.philippe@gmail.com
								</ListItemContent>
							</ListItem>
						</List>
					</div>
				</Cell>
			</Grid>
		</div>
	);
};

export default Contact;
