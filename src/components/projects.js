import React, { Component } from "react";
import {
	Tabs,
	Tab,
	Grid,
	Cell,
	Card,
	CardTitle,
	CardText,
	CardActions,
	Button,
	CardMenu,
	IconButton
} from "react-mdl";

class Projects extends React.Component {
	constructor(props) {
		super(props);
		this.state = { activeTab: 0 };
	}

	toggleCategories() {
		if (this.state.activeTab === 0) {
			return (
				<div className="projects-grid">
					<Card
						shadow={5}
						style={{ minWidth: "450", margin: "auto" }}
					>
						<CardTitle
							style={{
								color: "black",
								height: "176px"
							}}
						>
							<img
								src="images/caps1.PNG"
								style={{
									width: "100%",
									margin: "auto"
								}}
								cover
							/>
						</CardTitle>
						<CardText>
							A static website using HTML5, CSS3, Bootstrap4 and
							Mobile Responsive Design.
						</CardText>
						<CardActions border className="buttons-link">
							<Button colored>
								{" "}
								<a
									href="https://gitlab.com/tuitt/students/batch43/philippe-mallari/capstoneproject1"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Gitlab
								</a>
							</Button>
							<Button colored>
								<a
									href="https://github.com/PhilippeMallari?tab=repositories"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Github
								</a>
							</Button>
							<Button colored>
								<a
									href=" https://philippemallari.gitlab.io/capstone1-project"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Live Demo
								</a>
							</Button>
						</CardActions>
					</Card>
				</div>
			);
		} else if (this.state.activeTab === 1) {
			return (
				<div className="projects-grid">
					<Card
						shadow={5}
						style={{ minWidth: "450", margin: "auto" }}
					>
						<CardTitle
							style={{
								color: "black",
								height: "176px"
							}}
						>
							<img
								src="images/caps2.PNG"
								style={{
									width: "100%",
									margin: "auto"
								}}
								cover
							/>
						</CardTitle>
						<CardText>
							Asset Management System where users can request a
							car and admin can perform CRUD functionalities ex.
							Add assets, Approve and Reject Transactions.
						</CardText>
						<CardActions border className="buttons-link">
							<Button colored>
								{" "}
								<a
									href="https://gitlab.com/tuitt/students/batch43/philippe-mallari/capstone-project2"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Gitlab
								</a>
							</Button>
							<Button colored>
								<a
									href="https://github.com/PhilippeMallari?tab=repositories"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Github
								</a>
							</Button>
							<Button colored>
								<a
									href="http://rent-a-vehicle-now.herokuapp.com/login"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Live Demo
								</a>
							</Button>
						</CardActions>
					</Card>
				</div>
			);
		} else if (this.state.activeTab === 2) {
			return (
				<div className="projects-grid">
					<Card
						shadow={5}
						style={{ minWidth: "450", margin: "auto" }}
					>
						<CardTitle
							style={{
								color: "black",
								height: "176px"
							}}
						>
							<img
								src="images/caps3.png"
								style={{
									width: "100%",
									margin: "auto"
								}}
								cover
							/>
						</CardTitle>
						<CardText style={{ margin: "auto", marginTop: "60px" }}>
							Booking system for montalban resort. It includes
							room CRUD functions, user booking ang credential
							management.
						</CardText>
						<CardActions border className="buttons-link">
							<Button colored>
								{" "}
								<a
									href="https://gitlab.com/PhilippeMallari/capstone-3"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Gitlab
								</a>
							</Button>
							<Button colored>
								<a
									href="https://github.com/PhilippeMallari?tab=repositories"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Github
								</a>
							</Button>
							<Button colored>
								<a
									href="https://montalban-resort.herokuapp.com/"
									target="_blank"
									style={{
										textDecoration: "none",
										color: "blue"
									}}
								>
									{" "}
									Live Demo
								</a>
							</Button>
						</CardActions>
					</Card>
				</div>
			);
		}
	}

	render() {
		return (
			<div className="demo-tabs">
				<Tabs
					activeTab={this.state.activeTab}
					onChange={tabId => this.setState({ activeTab: tabId })}
					ripple
				>
					<Tab>HTML/CSS/Bootstrap Projects</Tab>
					<Tab>PHP/Laravel Projects</Tab>
					<Tab>MERN Projects</Tab>
				</Tabs>

				<Grid>
					<Cell col={12}>
						<div className="content">{this.toggleCategories()}</div>
					</Cell>
				</Grid>
			</div>
		);
	}
}

export default Projects;
