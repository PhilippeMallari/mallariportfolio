import React, { Component } from "react";
import { Grid, Cell } from "react-mdl";

const About = () => {
	return (
		<div style={{ width: "100%", margin: "auto" }}>
			<Grid className="about-grid">
				<Cell col={12}>
					<img
						src="images/me.PNG"
						alt="avatar"
						className="avatar-img"
					/>
					<div className="banner-text">
						<h1>Full Stack Web Developer</h1>

						<hr />
						<p>
							Hi! I'm Philippe Mallari, I'm passionate about web
							developing and technology on the side I also love
							sports and playing RTS games! Collaborate with me!
							Let's turn your ideas into reality.
						</p>
						<div className="social-links">
							<a
								href="https://www.linkedin.com/in/philippe-mallari-b9440b192/"
								rel="noopener noreferrer"
								target="_blank"
							>
								<i className="fab fa-linkedin-in right"></i>
							</a>

							<a
								href="https://github.com/PhilippeMallari?tab=repositories"
								rel="noopener noreferrer"
								target="_blank"
							>
								<i className="fab fa-github right"></i>
							</a>

							<a
								href="https://gitlab.com/dashboard/projects"
								rel="noopener noreferrer"
								target="_blank"
							>
								<i className="fab fa-gitlab right"></i>
							</a>
						</div>
						<hr style={{ marginTop: "30px" }} />
						<h1 id="technologies-header">TECHNOLOGIES</h1>
						<div className="technology-icons">
							<i class="fab fa-html5"></i>
							<i class="fab fa-css3-alt left"></i>
							<i class="fab fa-js left"></i>
							<i class="fab fa-bootstrap left"></i>
						</div>
						<div className="technology-icons">
							<i class="devicon-jquery-plain"></i>
							<i class="devicon-linux-plain"></i>
							<i class="devicon-mysql-plain-wordmark"></i>
							<i class="devicon-php-plain"></i>
						</div>
						<div className="technology-icons">
							<i class="devicon-laravel-plain-wordmark"></i>
							<i class="devicon-heroku-original"></i>
							<i class="devicon-mongodb-plain"></i>
							<i class="devicon-express-original"></i>
						</div>
						<div className="technology-icons">
							<i class="devicon-react-original"></i>
							<i class="devicon-nodejs-plain-wordmark"></i>
							<i class="devicon-git-plain"></i>
							<i class="devicon-github-plain"></i>
						</div>
						<div className="technology-icons">
							<i class="fab fa-gitlab"></i>
						</div>
					</div>
				</Cell>
			</Grid>
		</div>
	);
};

export default About;
